<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>image profile file upload on Change Profile feature</description>
   <name>fileUpload</name>
   <tag></tag>
   <elementGuidId>2d1cdc9c-c74d-4d54-bd1e-9f38325288dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@type=&quot;file&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
