<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Fullname                               _a7cccf</name>
   <tag></tag>
   <elementGuidId>ed931cc6-baf8-4a7c-9231-50379c7c59de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.py-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8f6e5a4d-157f-4f2c-b2b5-1ca68bc4d015</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>py-4</value>
      <webElementGuid>a54f10ba-03f6-4b18-aa2f-d71024e9fa5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                </value>
      <webElementGuid>0979f5c5-da6e-4284-aa5d-16732f28184d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]</value>
      <webElementGuid>05d43f3a-171d-4d3a-bf2d-049a4ab21b46</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]</value>
      <webElementGuid>1dec8adf-f831-4d04-90f6-4d3b27ec6a21</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Invoice'])[1]/following::div[10]</value>
      <webElementGuid>93ab375c-21c1-4049-b25e-011dbd0d99c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]</value>
      <webElementGuid>b5d940d7-33ef-4dd9-a463-7120490dbc2b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                ' or . = '

                                    
                                        Fullname
                                        
                                        
                                        

                                    

                                                                        
                                        Email
                                        
                                        
                                    
                                    
                                        Phone
                                        
                                        
                                            

                                    
       
                                    
                                        BirthDay
                                        
                                        
                                                                            
                                    
                                        
                                        Cancel
                                        Save Changes
                                    



                                ')]</value>
      <webElementGuid>b148f915-941d-44a2-bc38-0b391c0ca9b0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
