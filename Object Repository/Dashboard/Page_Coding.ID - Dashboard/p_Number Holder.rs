<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Number Holder</name>
   <tag></tag>
   <elementGuidId>2995657c-bcf4-4b2e-bcc8-e8c84b3f7f3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div/form/p[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b36b1247-e8f7-4dac-8ad6-338e3e91e190</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>clearfix</value>
      <webElementGuid>821d9d99-09ca-47bd-850f-dbf86ff31308</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            Number Holder
                                        
                                        
                                            
                                        
                                    </value>
      <webElementGuid>2fe35b1a-ecde-4f13-bc13-bc729b8d1b76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;py-4&quot;]/form[1]/p[@class=&quot;clearfix&quot;]</value>
      <webElementGuid>91cce6c7-e9b5-4582-8d6f-c6a2048e3c8a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/div/form/p[3]</value>
      <webElementGuid>293630e1-281b-44b8-89ee-ace0179b15f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Name Holder'])[1]/following::p[1]</value>
      <webElementGuid>5dc58966-6d5f-4582-bb2e-4b13f5ebb047</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ovo / Gopay'])[1]/following::p[2]</value>
      <webElementGuid>6b55c989-4bd1-41ba-83b3-000d253e2c71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Submit'])[1]/preceding::p[1]</value>
      <webElementGuid>1bd36870-9763-4984-9806-a74ab416a05a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[3]</value>
      <webElementGuid>366b3a31-3adb-4056-8f75-2efbed4b31d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                                        
                                            Number Holder
                                        
                                        
                                            
                                        
                                    ' or . = '
                                        
                                            Number Holder
                                        
                                        
                                            
                                        
                                    ')]</value>
      <webElementGuid>ccbae4be-93d9-4acd-9a3c-0b778df2a60a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
