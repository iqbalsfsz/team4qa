<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_My                                      _41f013</name>
   <tag></tag>
   <elementGuidId>9bf7b42d-04c7-4e62-9ae1-03ee8a5b2114</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>d028a081-7be6-4316-ae6f-d05bccf80bb0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>___class_+?26___</value>
      <webElementGuid>1e972d20-cd17-44aa-99c2-e28480fb24ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                </value>
      <webElementGuid>a507ee12-4343-4626-b701-8972ee33cbdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]</value>
      <webElementGuid>e6b9e128-16c7-413c-9aff-92ec79bb532c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]</value>
      <webElementGuid>958de9d0-dcb5-43e9-9b2c-d5a3d162dcb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::li[1]</value>
      <webElementGuid>d37f0c1c-06f0-4450-9a24-9fc2deb38eae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::li[2]</value>
      <webElementGuid>5fc43557-7c9f-4ead-bcca-272948273908</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]</value>
      <webElementGuid>d2058967-a043-43b9-9e56-2ddf6e0387a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ' or . = '
                                                        
                                                    
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    
                                                ')]</value>
      <webElementGuid>2c21766b-5655-43ae-b50a-a2388feefb2e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
