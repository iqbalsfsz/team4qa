<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>price_modal_total_2m</name>
   <tag></tag>
   <elementGuidId>9e0dc70e-0761-4965-9dbc-f62ccabb1fbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[@id='subtotalModal']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#subtotalModal</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>bee609d6-aa31-4bc2-896c-f29fab417425</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-modal-col    </value>
      <webElementGuid>a2418e66-c6c5-4b9d-8abb-da1f9def9413</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>subtotalModal</value>
      <webElementGuid>5c58f0f9-1a25-407d-981b-aa343735e90d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>2.500.000</value>
      <webElementGuid>449fdba4-12ff-4086-8d72-e6622363e2c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;subtotalModal&quot;)</value>
      <webElementGuid>174f4e89-328f-4fb0-b2f7-94b360d6fc44</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[@id='subtotalModal']</value>
      <webElementGuid>68dcffee-5eab-4db0-9e6c-529d22b9de0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table/tbody/tr/td[2]</value>
      <webElementGuid>d01264b9-29ec-4e1f-ae42-4158314f373f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Price'])[1]/following::td[1]</value>
      <webElementGuid>ee023927-6ce5-4065-88a1-bb66fe4c3e5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detail Pembayaran'])[1]/following::td[2]</value>
      <webElementGuid>6670b4d2-6333-4b1e-b00b-1c41becd679c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Discount Referral / Voucher'])[1]/preceding::td[1]</value>
      <webElementGuid>85849af5-d2d6-4dea-bcae-9ee9bc531a5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[1]/preceding::td[2]</value>
      <webElementGuid>65af23d2-db7d-4870-96c6-cd23b42ff44e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='2.500.000']/parent::*</value>
      <webElementGuid>5c6ac797-7825-457f-ad71-34f9aacb9886</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]</value>
      <webElementGuid>9a3a23e2-f362-4d43-ba60-7e053c5ab454</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[@id = 'subtotalModal' and (text() = '2.500.000' or . = '2.500.000')]</value>
      <webElementGuid>4b09d02e-22a9-41e6-89dd-f0ecb2e4c26e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
