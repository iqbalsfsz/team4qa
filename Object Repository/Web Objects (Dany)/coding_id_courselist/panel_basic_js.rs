<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>panel_basic_js</name>
   <tag></tag>
   <elementGuidId>7c2d4c60-13a6-438d-8140-cdb45c680910</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='containerCard']/div/a/div[2]/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.titleCourse</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>dd72d28c-627c-4062-b1b8-01b8fa9d40c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleCourse</value>
      <webElementGuid>85e0d7f4-9737-4adb-8896-a70b57965915</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Basic Programming with Javascript </value>
      <webElementGuid>adb97011-e347-465c-a7da-9cbf560483bd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;containerCard&quot;]/div[@class=&quot;cardOuter&quot;]/a[1]/div[@class=&quot;cardBody&quot;]/h3[@class=&quot;titleCourse&quot;]</value>
      <webElementGuid>98d40fa0-5f43-4c10-92aa-aae650287e42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='containerCard']/div/a/div[2]/h3</value>
      <webElementGuid>ee8d85e8-6e0a-48a8-b12f-6fad6f805be8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Terbaru'])[1]/following::h3[2]</value>
      <webElementGuid>4cce276d-fbb0-4cf4-aca1-ebad44123f38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Online Courses'])[1]/following::h3[2]</value>
      <webElementGuid>1c2954a2-0f7d-45ff-b83a-9e2a333098c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zakka,'])[1]/preceding::h3[1]</value>
      <webElementGuid>2da73dd3-9962-4685-8ac0-14d9056047f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.500.000'])[1]/preceding::h3[1]</value>
      <webElementGuid>46c389c2-c092-48db-90f2-670c02ad199c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Basic Programming with Javascript']/parent::*</value>
      <webElementGuid>7075286c-c6b6-4922-acc2-50024ec8bcfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h3</value>
      <webElementGuid>71970220-9c15-4296-941f-61dfd83bb88c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Basic Programming with Javascript ' or . = 'Basic Programming with Javascript ')]</value>
      <webElementGuid>355627e6-1efb-4a8d-8fdc-9cab53181e7a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
