<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invoice_status_cancel</name>
   <tag></tag>
   <elementGuidId>8d5b4930-5902-44dc-9cf1-ae180c3017a9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.badge.badge-warning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='badge-status']/h5/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>87027d63-84ad-4418-8a75-5529c82a6289</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>badge badge-warning</value>
      <webElementGuid>dd1944e6-1050-41b2-a293-3db3f45ffc37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Cancel by User</value>
      <webElementGuid>ac03b87d-78f1-4a96-9524-83987248405f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;badge-status&quot;)/h5[1]/span[@class=&quot;badge badge-warning&quot;]</value>
      <webElementGuid>8bf4e9a6-bab4-405b-9f48-851bd21e3f5f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='badge-status']/h5/span</value>
      <webElementGuid>02573de5-21e3-4609-905a-328a9050ab6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5/span</value>
      <webElementGuid>d04a6ca3-3bee-4ac3-ba59-587d2ac7c33b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Cancel by User' or . = 'Cancel by User')]</value>
      <webElementGuid>eab46ce9-231c-4e6d-932d-6b00995979e8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
