<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>invoice_status_complete</name>
   <tag></tag>
   <elementGuidId>b5f6be63-ae1f-4b40-b0a7-79c034d0974b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.badge.badge-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='badge-status']/h5/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a0750d11-de57-4a7a-89a4-f052798bec07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>badge badge-success</value>
      <webElementGuid>8188546f-b766-4a2c-959f-46e3ef3ef44c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Completed</value>
      <webElementGuid>db5286da-efc8-4948-9ab2-574750cdef89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;badge-status&quot;)/h5[1]/span[@class=&quot;badge badge-success&quot;]</value>
      <webElementGuid>b421c7ce-ca36-467e-b26e-5a9c1ed569c1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='badge-status']/h5/span</value>
      <webElementGuid>9f3b31d1-cee5-4652-bb26-51386124b08a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h5/span</value>
      <webElementGuid>48405f81-20fc-4c34-aeb4-1d72ac606b9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Completed' or . = 'Completed')]</value>
      <webElementGuid>a5451b4d-0e40-4617-a72f-36d508152e3b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
