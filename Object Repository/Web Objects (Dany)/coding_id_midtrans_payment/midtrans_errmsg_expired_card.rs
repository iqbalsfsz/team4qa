<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>midtrans_errmsg_expired_card</name>
   <tag></tag>
   <elementGuidId>944bce00-0c1a-42a9-9ded-e2e1198c3fe4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-expiry-cvv > div.card-warning.text-failed</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div/div/div[3]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>7e5cec2f-d647-4917-9db3-bcf1eb32eff9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-warning text-failed</value>
      <webElementGuid>534c46ee-4b66-47c4-ba49-c03bf710463f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invalid expiry.</value>
      <webElementGuid>0d8025cf-c54c-4579-99ae-8b342d561180</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[@class=&quot;credit-card__wrapper&quot;]/div[@class=&quot;credit-card__content&quot;]/div[@class=&quot;credit-card__info&quot;]/div[@class=&quot;card-expiry-cvv&quot;]/div[@class=&quot;card-warning text-failed&quot;]</value>
      <webElementGuid>69d3dac6-6e46-44ed-a5a7-cbfa56b7f43c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web Objects (Dany)/coding_id_midtrans_payment/midtrans_iframe_main</value>
      <webElementGuid>0d23150d-469e-4cd9-b50a-87ebd658b8be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div/div/div[3]/div/div</value>
      <webElementGuid>bdc5d078-db01-4992-86f4-1703030b3e15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Expiration date'])[1]/following::div[1]</value>
      <webElementGuid>05125be9-4fac-4b4c-83bd-8414d4a27ca0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make sure your card info is correct.'])[1]/following::div[3]</value>
      <webElementGuid>36b6716c-99df-4ea8-8ff0-53fa886e869e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CVV'])[1]/preceding::div[1]</value>
      <webElementGuid>e559941e-2184-45b1-a38b-8108ce51e35d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pay now'])[1]/preceding::div[3]</value>
      <webElementGuid>b2092363-e0dd-4f0c-aa8a-5f0f3ca0de5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invalid expiry.']/parent::*</value>
      <webElementGuid>1e2d5bc1-d3e6-44b0-8e2e-a0ba025a0d59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div</value>
      <webElementGuid>c2664248-84b2-4e94-bfef-163558ae3f0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Invalid expiry.' or . = 'Invalid expiry.')]</value>
      <webElementGuid>511813b1-d85a-487b-a745-927d63a37418</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
