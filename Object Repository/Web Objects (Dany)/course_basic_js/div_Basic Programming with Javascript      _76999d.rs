<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Basic Programming with Javascript      _76999d</name>
   <tag></tag>
   <elementGuidId>94057b44-e337-4459-9800-46d5d746823c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='jumbotronCourseDetail']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#jumbotronCourseDetail</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>be7e776c-c2e4-4103-964e-20b0248211da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>jumbotronCourseDetail</value>
      <webElementGuid>02fbe784-94d8-4e9f-af7a-ca4691ecfa67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        Basic Programming with Javascript
        
        Pada course ini kamu akan belajar basic-basic programming, dimulai dari pengenalan bahasa programming, prinsip Object-Oriented Programming dan bahkan Database. Kamu akan dikenalkan dengan bahasa JavaScript yang merupakan sebuah object-oriented computer programming language yang bisanya digunakan untuk memberikan fungsi interaktif pada halaman web. Namun karena berkembangnya zaman, JavaScript sekarang bisa digunakan untuk membuat aplikasi web, desktop maupun mobile. Kalian juga akan diajarkan menganai konsep dari Object Oriented Programming yang sudah banyak dijadikan standart pengunaan suatu framework. Selain itu, kamu juga akan belajar menggunakan Database MySql dan bagaimana menggunakan query pada MySql.
        

                                                        
                        Take
                            to Cart
                        
                                        

    
        
    
</value>
      <webElementGuid>3eeee577-2052-4c99-a63c-4eb7d2c33035</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronCourseDetail&quot;)</value>
      <webElementGuid>742f31a6-35e4-434a-9a9b-d3b602dd9dc3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='jumbotronCourseDetail']</value>
      <webElementGuid>ad0b6d60-e62f-4ac2-aa89-9f09d92f5bad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div</value>
      <webElementGuid>87e83837-5707-4789-a61c-7956caaf2d6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'jumbotronCourseDetail' and (text() = '
    
        Basic Programming with Javascript
        
        Pada course ini kamu akan belajar basic-basic programming, dimulai dari pengenalan bahasa programming, prinsip Object-Oriented Programming dan bahkan Database. Kamu akan dikenalkan dengan bahasa JavaScript yang merupakan sebuah object-oriented computer programming language yang bisanya digunakan untuk memberikan fungsi interaktif pada halaman web. Namun karena berkembangnya zaman, JavaScript sekarang bisa digunakan untuk membuat aplikasi web, desktop maupun mobile. Kalian juga akan diajarkan menganai konsep dari Object Oriented Programming yang sudah banyak dijadikan standart pengunaan suatu framework. Selain itu, kamu juga akan belajar menggunakan Database MySql dan bagaimana menggunakan query pada MySql.
        

                                                        
                        Take
                            to Cart
                        
                                        

    
        
    
' or . = '
    
        Basic Programming with Javascript
        
        Pada course ini kamu akan belajar basic-basic programming, dimulai dari pengenalan bahasa programming, prinsip Object-Oriented Programming dan bahkan Database. Kamu akan dikenalkan dengan bahasa JavaScript yang merupakan sebuah object-oriented computer programming language yang bisanya digunakan untuk memberikan fungsi interaktif pada halaman web. Namun karena berkembangnya zaman, JavaScript sekarang bisa digunakan untuk membuat aplikasi web, desktop maupun mobile. Kalian juga akan diajarkan menganai konsep dari Object Oriented Programming yang sudah banyak dijadikan standart pengunaan suatu framework. Selain itu, kamu juga akan belajar menggunakan Database MySql dan bagaimana menggunakan query pada MySql.
        

                                                        
                        Take
                            to Cart
                        
                                        

    
        
    
')]</value>
      <webElementGuid>e1f0b123-ab18-4d01-b170-374ddfab6ecf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
