import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'arifiqbal.loginBefore.loginBeforeEach'()

WebUI.navigateToUrl('https://demo-app.online/dashboard/profile')

WebUI.click(findTestObject('Object Repository/Dashboard/Page_Coding.ID - Dashboard/a_Edit Profile'))

WebUI.setText(findTestObject('Change Profile/Page_Coding.ID - Dashboard/input_Fullname_name'), 'Arif Iqbal')

WebUI.setText(findTestObject('Change Profile/Page_Coding.ID - Dashboard/input_Phone_whatsapp'), 'phone!@#')

WebUI.click(findTestObject('Change Profile/Page_Coding.ID - Dashboard/button_Save Changes'))

WebUI.verifyElementVisible(findTestObject('Dashboard/Page_Coding.ID - Dashboard/strong_The whatsapp must be a number'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.closeBrowser()

