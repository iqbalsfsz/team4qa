import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/login')

WebUI.setText(findTestObject('Object Repository/Web Objects (Dany)/coding_id_login/login_email'), 'fmdemo2@mailinator.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Web Objects (Dany)/coding_id_login/login_password'), '9Y9i2tV1HPqCP0OPi/5LbA==')

WebUI.click(findTestObject('Object Repository/Web Objects (Dany)/coding_id_login/login_button'))

WebUI.waitForPageLoad(0)

WebUI.navigateToUrl('https://demo-app.online/event/day-4-workshop-flya6jmot1vq')

WebUI.click(findTestObject('Object Repository/Web Objects (Dany)/event_day_4_workshop/button_buy_ticket')) // tidak perlu cek pesan konfirmasi untuk event, vs. course

WebUI.navigateToUrl('https://demo-app.online/course/basic-programming-with-javascript-eci19jm45dtb')
//hindari konflik dengan WebBeli-06/08
if (WebUI.verifyElementVisible(findTestObject('Object Repository/Web Objects (Dany)/course_basic_js/button_take_course'), FailureHandling.OPTIONAL))
	{
		WebUI.click(findTestObject('Object Repository/Web Objects (Dany)/course_basic_js/button_take_course'))
		WebUI.waitForElementVisible(findTestObject('Object Repository/Web Objects (Dany)/event_day_4_workshop/msg_added_to_cart'), 
    0)
		WebUI.navigateToUrl('https://demo-app.online/view_cart')
	}
	else if (WebUI.verifyElementVisible(findTestObject('Object Repository/Web Objects (Dany)/course_basic_js/button_goto_cart_already'), FailureHandling.OPTIONAL))
	{
		WebUI.navigateToUrl('https://demo-app.online/view_cart')
	}

WebUI.check(findTestObject('Object Repository/Web Objects (Dany)/coding_id_cart/item_checkbox'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Web Objects (Dany)/coding_id_cart/cart_button_checkout'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Web Objects (Dany)/coding_id_cart/price_modal_total_2m'))

