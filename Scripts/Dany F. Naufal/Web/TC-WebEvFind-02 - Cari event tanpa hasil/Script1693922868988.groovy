import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keysimport
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory

def testdata = TestDataFactory.findTestData('Data Files/unsearchables')

for (def row = 1; row <= testdata.getRowNumbers(); row++) {
    def keyword = testdata.getValue('keyword', row)

    WebUI.openBrowser('')

    WebUI.navigateToUrl('https://demo-app.online/event')

    WebUI.setText(findTestObject('Web Objects (Dany)/coding_id_eventlist/events_search_box'), keyword)

    WebUI.verifyTextPresent('No available event', false) // regex true = gagal dengan keyword "bruh"?
}

