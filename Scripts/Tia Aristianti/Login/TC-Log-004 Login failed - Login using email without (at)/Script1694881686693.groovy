import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/login')

WebUI.maximizeWindow()

WebUI.verifyElementText(findTestObject('Page_Masuk untuk dapatkan akses di Coding.ID/label_Email'), 'Email')

WebUI.setText(findTestObject('Object Repository/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'), 'tiaaristiantigmail.com')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Masuk untuk dapatkan akses di Coding.ID/label_Kata                                 _0e5ced'), 
    'Kata Sandi')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'), 
    'pIuQd5VpA2TnWpIxJj1GbQ==')

WebUI.verifyElementClickable(findTestObject('Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

WebUI.verifyElementText(findTestObject('Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'), 'Login')

WebUI.click(findTestObject('Object Repository/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))

WebUI.verifyElementNotPresent(findTestObject('Page_Be a Profressional Talent with Coding.ID/h1_Coding Bootcamp            Tech Talent B_bc94c8'), 
    0)

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.closeBrowser()

