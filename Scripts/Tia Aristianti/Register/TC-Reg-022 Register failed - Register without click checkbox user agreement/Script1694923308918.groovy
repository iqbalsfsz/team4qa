import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar?')

WebUI.maximizeWindow()

WebUI.verifyElementText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/label_Nama'), 'Nama')

WebUI.setText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 'tiaaristi')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/label_Tanggal lahir'), 
    'Tanggal lahir')

WebUI.setText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    '31-Dec-1998')

WebUI.verifyElementText(findTestObject('Page_Buat akun dan dapatkan akses di Coding.ID/label_E-Mail'), 'E-Mail')

WebUI.setText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), 'tiaa.ristianti@gmail.com')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/label_Whatsapp'), 
    'Whatsapp')

WebUI.setText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), 
    '08198989898')

WebUI.verifyElementText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/label_Kata Sandi'), 
    'Kata Sandi')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'), 
    'IiiKlAiWmeOR474j7JBH5w==')

WebUI.verifyElementText(findTestObject('Page_Buat akun dan dapatkan akses di Coding.ID/label_Konfirmasi kata sandi'), 'Konfirmasi kata sandi')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'), 
    'IiiKlAiWmeOR474j7JBH5w==')

WebUI.verifyElementClickable(findTestObject('Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyElementText(findTestObject('Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'), 'Daftar')

WebUI.click(findTestObject('Object Repository/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyElementNotPresent(findTestObject('Page_Coding.ID - Verifikasi Email/span_Verifikasi Email'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.closeBrowser()

