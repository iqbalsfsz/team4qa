import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\tiaar\\Katalon Studio\\samplemobiletest\\androidapp\\DemoAppV2.apk', false)

Mobile.tap(findTestObject('CodingIDObjects/HomePage/btnLoginHere'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnRegister'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpnama'), 'tiaa')

Mobile.clearText(findTestObject('CodingIDObjects/Register Page/inpnama'), 0)

Mobile.verifyElementExist(findTestObject('CodingIDObjects/Register Page/alert-ertemptyName'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/inpTglLahir'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnCancel'), 0)

Mobile.verifyElementExist(findTestObject('CodingIDObjects/Register Page/Alert-emptyBirtdate'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/emailnewselector'), 'tiaa@gmai.com')

Mobile.clearText(findTestObject('CodingIDObjects/Register Page/emailnewselector'), 0)

Mobile.verifyElementExist(findTestObject('CodingIDObjects/Register Page/alert-emptyEmail'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpWhatsapp'), '081232132132')

Mobile.clearText(findTestObject('CodingIDObjects/Register Page/inpWhatsapp'), 0)

Mobile.verifyElementExist(findTestObject('alert-WArequired'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpPassword'), '1234qwer')

Mobile.clearText(findTestObject('CodingIDObjects/Register Page/inpPassword'), 0)

Mobile.verifyElementExist(findTestObject('CodingIDObjects/Register Page/alert-SKpassword'), 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

