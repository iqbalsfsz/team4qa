import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\tiaar\\Katalon Studio\\samplemobiletest\\androidapp\\DemoAppV2.apk', false)

Mobile.tap(findTestObject('CodingIDObjects/HomePage/btnLoginHere'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnRegister'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpnama'), 'Tia Aristianti')

Mobile.tap(findTestObject('CodingIDObjects/Register Page/inpTglLahir'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnOK'), 0)

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/emailnewselector'), 'tiaaris@gmail.com')

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpWhatsapp'), '081234567891')

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpPassword'), '123')

Mobile.sendKeys(findTestObject('CodingIDObjects/Register Page/inpConfPassword'), '123')

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnCheckbox'), 0)

Mobile.tap(findTestObject('CodingIDObjects/Register Page/btnDaftar'), 0)

Mobile.verifyElementExist(findTestObject('CodingIDObjects/Register Page/alert-SKpassword'), 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

