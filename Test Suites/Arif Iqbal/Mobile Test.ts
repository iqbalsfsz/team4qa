<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Mobile Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>13746c39-3223-4cc3-9acf-55574dc1d290</testSuiteGuid>
   <testCaseLink>
      <guid>16239b90-b618-45ae-8914-066bed19e6cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC - Login</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1072f16e-97c9-4aa4-a580-e7b17ea6cf14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-01 - Verify Change Profile Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ced27d2d-9393-4621-a4c1-5c44c3dd9ceb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-02 - Verify Change Profile Failed - Invalid FullName (symbol and number)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b6381098-e11f-4e53-88d2-94615098be54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-03 - Verify Change Profile Failed - Blank Full Name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3b91630a-7cb5-4324-8113-dd056f3b46be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-04 - Verify Change Profile Failed - Invalid Phone Number (symbol and letter)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e9be4803-13d2-4a7a-b1bf-b8bbbbf39bb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-05 - Verify Change Profile Failed - Invalid Phone Number (outside of range)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2648056a-da3e-45d0-8d73-0584c511b92e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-06 - Verify Change Profile Failed - Blank Phone Number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d2cda64f-e407-44cd-896c-eb4dadacefc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-08 - Verify Change Profile Failed - Blank Birthday</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>af3e75c0-5395-4b0f-a2f8-20b2e2c49780</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCP-09 - Verify Cancel Change Profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bf319813-bdb8-4345-841e-500b65a24078</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCpass-02 - Verify Change Password Failed - Invalid Old Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>056c4e5c-906d-42ed-b6ff-d52b0e9fb842</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCpass-03 - Verify Change Password Failed - Invalid Confirmation Password - Copy</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2d2a3df4-6263-4637-9909-700f57ea2980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/TC-MoCpass-03 - Verify Change Password Failed - Invalid Confirmation Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
