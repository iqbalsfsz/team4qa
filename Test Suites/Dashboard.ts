<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Dashboard</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f88e7d35-1dc3-422a-88aa-a6915aa3e951</testSuiteGuid>
   <testCaseLink>
      <guid>9c60ab1a-f86a-4b95-8055-37ac86b0c302</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-01 - Verify Add Billing Info Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>06228857-68e6-42b3-b584-2447bc02df84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-02 - Invalid emoney</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5946f740-9851-49bf-a718-457856431c31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-03 - Verify Add Billing Info Failed - Invalid Name Holder (symbol and number)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f3afea5e-09c8-411c-92cd-4edf6a6ce158</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-04 - Verify Add Billing Info Failed - Blank Name Holder Field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4355f64c-6c3c-4690-b67a-1bc20ae0bde0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-05 - Verify Add Billing Info Failed - Invalid Number Holder (symbol and letter)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>085954ce-cb1d-4748-b14c-adb5e4309ddf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-06 - Verify Add Billing Info Failed - Invalid Number Holder (Outside of Range)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d0cbca00-7fa2-4470-bd8f-6bc739c798ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-07 - Verify Add Billing Info Failed - Blank Number Holder Field</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2e879d33-6125-44f1-bd94-44b986072635</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Billing Account/TC-Bill-08 - Verify Add Billing Info Failed - No Change</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fc34bbba-1e49-4831-9336-0e547316b63a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Password/TC-Cpass-02 - Verify Change Password Failed - Invalid Old Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2c698bae-a000-445a-be71-cc37e0eb2b31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Password/TC-Cpass-03 - Verify Change Password Failed - Invalid Confirmation Password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8a9007b7-2daf-4a25-a4b6-bdb8862671ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Password/TC-Cpass-04 - Verify Change Password Failed - Invalid New Password (Outside of Range)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>38d26462-5835-4b96-a5f9-5e7565a993cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Password/TC-Cpass-06 - Verify Change Password Failed - No Change</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>61471447-9f6f-4587-8571-48fa90fcae57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-01 - Verify Change Profile Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b91ff4aa-fdb2-4298-adca-3021a6096301</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-02 - Verify Change Profile Failed - Invalid FullName (symbol and number)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9b9e8648-18c6-423a-b36e-dff0860f9f75</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6531804f-fe8e-4232-bc48-e7531273c466</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>530b266c-e3ce-43f8-94fd-13cb8901be80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-03 - Verify Change Profile Failed - Blank Full Name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>881905d9-c72c-45e0-9154-476b590d00ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-04 - Verify Change Profile Failed - Invalid Phone Number (symbol and letter)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1057d69c-ef23-4343-91cc-ce3700be366b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-05 - Verify Change Profile Failed - Invalid Phone Number (outside of range)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dc41cffc-0785-45c2-8925-1d6727d6e273</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-07 - Verify Change Profile Failed - Blank Phone Number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>314b8077-79fa-499d-a7d1-a40335588d2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-10 - Verify Cancel Change Profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cb923ece-d053-4486-84ae-01468f9acec4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Dashboard/TC-DashB-01 - Verify Go To Dashboard Page Successfully</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>056dfe83-0b55-4c7b-9dca-5f279067f76e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Arif Iqbal/Change Profile/TC-CP-09 - Verify Change Profile Failed - Invalid Image File</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c08fcf8d-fa97-4ad0-b792-29d62788c107</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
