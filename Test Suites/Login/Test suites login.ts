<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suites login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>81a75a29-b357-4fc5-82b9-a59c9bc34f20</testSuiteGuid>
   <testCaseLink>
      <guid>fb8478cf-accd-4eda-8cc8-3f064d61a6d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-001 Login succeed - Login with valid data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ff007109-6817-442c-a3b2-5de54e87f523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-002 Login failed - Login using a blank email and password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7c5f7a02-10e4-46d9-a272-a577d96c289d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-003 Login failed - Login using invalid email and password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>2a9e9ecd-4efd-497b-8bea-15c83ea6d5c7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/testdata_login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>2a9e9ecd-4efd-497b-8bea-15c83ea6d5c7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>4fcefd6d-4b73-4849-8bf8-36fb0f3a761a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>2a9e9ecd-4efd-497b-8bea-15c83ea6d5c7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>f6f5c73d-ce24-4479-a31d-67f9156a6db0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d688b3c4-ac91-4238-b401-c11f67002781</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-004 Login failed - Login using email without (at)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4598193b-f952-4e90-a8f3-1a2d49e86af9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-005 Login failed - Login using email not complete</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>96e15e03-90f2-4ae1-b037-ba454c161ced</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-006 Login failed - Login using blank email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5d2f56b3-a66d-4193-8d54-7ef300ca88e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Login/TC-Log-007 Login failed - Login using blank password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
