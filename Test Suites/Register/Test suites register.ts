<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test suites register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>df51bdaf-423c-43fa-80c1-f884d705c04b</testSuiteGuid>
   <testCaseLink>
      <guid>dc434d9a-1f75-4512-97d3-280e4027717a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-001 Register succeed - Register with valid data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>849d8cf0-0779-4874-bead-aa60816e245a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-002 Register failed - Register with without (at)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>45c4d287-3e8e-4635-890f-a9cc3628f1c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-003 Register failed - Register with WA number less than 10 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e0e3f67a-7308-4429-98cc-6a3a855b64a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-004 Register failed - Register with WA number more than 12 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bd0703ab-3023-4195-8460-0a99d3ba9aa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-005 Register failed - Register with password less than 8 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>44b17b34-5b71-446a-8281-de7dd5d59381</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-006 Register failed - Register with all field blank</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ab717737-296c-4b1c-8553-6ceeb34525d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-007 Register failed - Register with name using all number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8ef0fd6d-a0b3-45f9-b4bc-ade932f02909</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-008 Register failed - Register with name using all special characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1a5fffe7-6468-4dee-96dd-37f2b1ab9783</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-009 Register failed - Register with blank name</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>73fb559a-df1d-4dec-be66-cd0e002ec6ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-010 Register failed - Register with the name using a space at the beginning</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>afa3c411-8540-4e2a-b84e-9632cb66700b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-011 Register failed - Register with email using all number</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fb05f613-3caa-4a49-8b06-fde990db12c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-012 Register failed - Register with email using special characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4371203e-2273-4aa7-b0ff-7edd2ec4cc1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-013 Register failed - Register with blank email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>473ab159-de3d-4793-9d11-94ff840feae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-014 Register failed - Register with email using a space at the beginning</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e5242dcd-95bd-4fc6-9ae2-058ccaa09258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-015 Register failed - Rgister with whatsapp contains leter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>73262815-15eb-4dcc-827b-3376b65013fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-016 Register failed - Register with blank whatsapp</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b7be3b31-053f-4ba3-a8a6-7b9da61e2b35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-017 Register failed - Register with number that is not registered in WhatsApp</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7b3d547e-a85f-40c1-8ffe-b92120ae5d14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-018 Register failed - Register with format WhatsApp number is invalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3226c30f-01dd-4042-8490-7832f16da23a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-019 Register failed - Register using registered email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5ecb49ae-3c4b-403e-8672-50024ed132c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-020 Register failed - Register with password confirmation not match</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4a65f2c6-0111-426a-b115-f76857c9bbda</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/testdata_register</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>4a65f2c6-0111-426a-b115-f76857c9bbda</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>d8e9b0a7-459c-4915-bad6-df51b4a2f326</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4a65f2c6-0111-426a-b115-f76857c9bbda</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password_confirmation</value>
         <variableId>4a754dcd-6ad8-442b-94c2-49cb9dd5b3f3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f659d72c-a133-4805-8608-d4de0f697009</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-021 Register failed - Register using registered email (email using(.))</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6d293cf4-b2f0-4696-844e-84a8627ee4a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-022 Register failed - Register without click checkbox user agreement</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>98bd8d55-b790-4766-8223-00dc5166d162</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Register/TC-Reg-023 Register failed - Register with user less than 7 years old</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
