<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suites Register</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>78716126-3b6d-4d10-8a59-edfaab5cd1e1</testSuiteGuid>
   <testCaseLink>
      <guid>bb089d4d-1b66-4443-b05a-9bad5a01703a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-001 Register Succeed - Register with valid data</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5bc14536-42d9-4f48-8332-e868b3226e8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-002 Register Failed - Register using email without (at)</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>04651e2f-441f-4ed2-9294-c6ab15323c24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-003 Register Failed - Register with WA number less than 9 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>646decc9-ad88-4d89-a7bd-25f5e3c0d751</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-004 Register Failed - Register with format WhatsApp number is invalid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>53a10a73-f1f8-48a4-8655-de990add209d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-005 Register Failed - Register with password contains all numbers</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>964b831c-ee91-4028-8748-5fb9ea84a9ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-006 Register Failed - Register with all letter password</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7bcecd73-91fd-4af2-b8b6-1f61f27f9a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-007 Register Failed - Register with all fields empty</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3dfa6490-ddc8-4c0f-971b-29350418a306</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-008 Register Failed - Register with password lest than 9 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bf009cbb-f5b4-44cc-857f-8fb35fe48e29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-009 Register Failed - Register without cllick checbox user agreement</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6a61da5d-2f1e-4799-a91c-9b1c7bf5eac0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tia Aristianti/Registermobile/TC-Reg-010 Register Failed - Register with password confirmation not match</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
